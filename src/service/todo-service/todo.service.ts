import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Todo } from 'src/model/todo.entity';

@Injectable()
export class TodoService {
  constructor(
    @InjectRepository(Todo) private todosRepository: Repository<Todo>,
  ) {}

  async getAllTodos(): Promise<Todo[]> {
    return await this.todosRepository.find();
  }

  async getTodoById(_id: number): Promise<Todo> {
    return await this.todosRepository.findOne({
      select: ['content', 'category', 'isUrgent', 'doneDate'],
      where: [{ id: _id }],
    });
  }

  async createTodo(todo: Todo) {
    this.todosRepository.save(todo);
  }

  async updateTodo(id: string, todo: Todo): Promise<Todo> {
    const editTodo = await this.todosRepository.findOneBy({ id: +id });
    console.log(editTodo);
    if (!editTodo) {
      throw new NotFoundException('Todo is not found!');
    }

    editTodo.category = todo.category;
    editTodo.content = todo.content;
    editTodo.isUrgent = todo.isUrgent;
    editTodo.doneDate = todo.doneDate;

    return await this.todosRepository.save(editTodo);
  }

  async deleteTodo(todo: Todo) {
    this.todosRepository.delete(todo);
  }
}
