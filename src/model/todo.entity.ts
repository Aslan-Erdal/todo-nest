import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

type CategoryType = "shopping" | "health" | "work" | "bills" | "cleaning" | "other";


@Entity()
export class Todo {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    
    content: string;

    @Column()
    category: CategoryType;

    @Column({ default: true })
    isUrgent: boolean;

    @Column({
        nullable: true,
    })
    doneDate: Date | null;
}