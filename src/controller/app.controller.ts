import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { Todo } from '../model/todo.entity';
import { TodoService } from '../service/todo-service/todo.service';

@Controller('todos')
export class AppController {
  constructor(private readonly todoService: TodoService) {}

  @Get()
  getTodos() {
    return this.todoService.getAllTodos();
  }

  @Get(':id')
  getTodo(@Param() params) {
    return this.todoService.getTodoById(params.id);
  }

  @Post()
  create(@Body() todo: Todo) {
    console.log(todo);
    return this.todoService.createTodo(todo);
  }

  @Put(':id')
  updateTodo(@Param('id') id: string, @Body() todo: Todo) {
    console.log(id);
    console.log(todo);
    return this.todoService.updateTodo(id, todo);
  }

  @Delete(':id')
  deleteTodo(@Param() params) {
    return this.todoService.deleteTodo(params.id);
  }
}
